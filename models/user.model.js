const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto');
const bcrypt = require('bcrypt');

// bcrypt config
const bcryptVars = {
  saltRounds : 2
}

const userSchema = new Schema({
  email: String,
  name: String,
  password: String
});



userSchema.pre('save', function(next, req, callback){
  console.log(this);
  let user = this;
  bcrypt.hash(this.password, bcryptVars.saltRounds, function(err, hash) {
    if(err){
      console.error(err);
      return next(err);
    }
    // Store hash in your password DB.
    console.log("Generated Hash -> ", hash);
    user.password = hash;
    next();
  });
})

/* generates random string of characters i.e salt */
const genRandomString = function(length){
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length);   /** return required number of characters */
}

module.exports = mongoose.model('user', userSchema);
