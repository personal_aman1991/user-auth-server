const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const usersRouter = require('./routes/users');

const cors = require('cors')
const app = express();
app.use(cors())
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

mongoose.connect('mongodb://localhost:27017/userAuthDb', { useNewUrlParser: true });

mongoose.connection.on('connected', function() {
    console.info('Connected to userAuthDb Database');
});

mongoose.connection.on('error', function(err) {
    console.error('Error connecting to userAuthDb Database: ' + err);
});



app.use('/api/v1/users', usersRouter);

module.exports = app;
