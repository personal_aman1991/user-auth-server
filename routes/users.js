const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const UserModel = require('../models/user.model');

/* GET users listing. */
router.get('/allUsers', function(req, res, next) {
  res.send('respond with a resource');
});

/* POST user creation. */
router.post('/register', function(req, res, next) {
  if(!req.body.name || !req.body.password){
    return res.status(400).json({success: false, message: 'Missing required fields.'})
  }
  UserModel.create(req.body)
          .then(newUser => {
              let _resObj = {
                success: true,
                message: 'New User generated',
                data: newUser
              }
              res.status(201).json(_resObj);
          }).catch(err => res.status(500).json(err));
});

/* POST user login. */
router.post('/login', function(req, res) {
  UserModel.findOne({email: req.body.email}).then(user => {
    bcrypt.compare(req.body.password, user.password, function(err, result) {
      if(err){
        console.error(err);
        return res.status(500).json({
          success: false,
          message: 'Something went wrong. Please try again after some time.'
        })
      }
      console.log("Comparing passwords -> ", result);
      if(result){
        let _resObj = {
          success: true,
          message: 'User Validated.',
          token: `NewToken${Math.random()*100000.2313}`
        }
        return res.status(200).json(_resObj);
      }else{
        let _resObj = {
          success: false,
          message: 'Wrong Password.'
        }
        return res.status(403).json(_resObj);
      }
    });
  })

})

module.exports = router;
